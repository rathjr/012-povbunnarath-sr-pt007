import React from 'react'

export default function Pagenotfound() {
    return (
        <div className="container">
           <h1 style={{textAlign:"center"}}>404 Page Not found</h1>
        </div>
    )
}
