import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Category from './views/Category';
import Mymenu from './components/Mymenu';
import Pagenotfound from './components/Pagenotfound';
export default function App() {
  return (
    <Router>
      <Mymenu/>
      <Switch>
        <Route path='/' exact component={Category}/>
        <Route path='*' component={Pagenotfound}/>
      </Switch>
    </Router>
  )
}

