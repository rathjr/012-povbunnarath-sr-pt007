import axios from "axios";

//Base URL for Category
export const APICat = axios.create({
  baseURL: "http://110.74.194.124:3034/api"
})

//fetch all cat
export const fetchAllCategory = async () => {
  try {
    const result = await APICat.get("/category")
    console.log("result Category", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("error Category", error);
  }
}

//fetch category by id
export const fetchCategoryById = async (id) => {
  try {
    const result = await APICat.get(`/category/${id}`)
    console.log("fetchCategoryById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("category error:", error);
  }
}

//delete category by ID
export const deleteCategoryById = async (id) => {
  try {
    const result = await APICat.delete(`/category/${id}`);
    console.log("deleteCatById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("deleteCatById Error:", error);
  }
}

//Add category to api
export const addCategory = async (category) => {
  try {
    const result = await APICat.post("/category", category);
    console.log("addCategory:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("addArticle Error:", error);
  }
}

//update category by ID
export const updateCategoryById = async (id, category) => {
  try {
    const result = await APICat.put(`/category/${id}`, category)
    console.log("updateCategoryById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("updateCategoryById Error:", error);
  }
}
